# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Its the XML examples and associated schema from our MarkupUK 2021 paper

### What are the schemas? ###

 * `diff3x.xsd` this is the primary schema for most of the examples - XML payload is CDATA
 * `deprecated/diff3xns.xsd` used for one of the examples (5-a) where the XML payload is XML
 * `deprecated/diff3nested.xsd` used for (4-a) where we discuss nested change - too complex so no further work

The `deprecated` directory contains schemas and examples that are discussed in the paper, but which do not consider worth progressing.

### How do I get set up? ###

* Clone 
* We've used XML model PIs and Oxygen should be able to validate

### Who do I talk to? ###

* nigel.whitaker@deltaxml.com, robin.lafontaine@deltaxml.com, or support@deltaxml.com
